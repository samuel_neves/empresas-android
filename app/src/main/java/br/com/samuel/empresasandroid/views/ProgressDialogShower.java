package br.com.samuel.empresasandroid.views;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * @author Samuel Neves
 *         Created on 09/03/18.
 */

class ProgressDialogShower {
    private ProgressDialog mProgressDialog;
    private Context context;

    ProgressDialogShower(Context context) {
        this.context = context;
    }

    void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Realizando Login");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
}
