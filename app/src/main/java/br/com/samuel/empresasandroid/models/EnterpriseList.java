package br.com.samuel.empresasandroid.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Samuel Neves
 *         Created on 07/03/18.
 */

public class EnterpriseList {
    @SerializedName("enterprises")
    private List<Enterprise> enterprises;

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }
}
