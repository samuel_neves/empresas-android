package br.com.samuel.empresasandroid.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.samuel.empresasandroid.viewmodels.EnterpriseViewModel;

public class Enterprise {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("email_enterprise")
    @Expose
    private String emailEnterprise;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("linkedin")
    @Expose
    private String linkedin;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("own_enterprise")
    @Expose
    private Boolean ownEnterprise;
    @SerializedName("enterprise_name")
    @Expose
    private String enterpriseName;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("share_price")
    @Expose
    private Integer sharePrice;
    @SerializedName("enterprise_type")
    @Expose
    private EnterpriseType enterpriseType;

    public static EnterpriseViewModel getErrorEnterprise() {
        EnterpriseViewModel enterprise = new EnterpriseViewModel();
        enterprise.setName("Error");
        enterprise.setDescription("Error");
        enterprise.setPhotoUrl("Error");
        return enterprise;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmailEnterprise() {
        return emailEnterprise;
    }

    public void setEmailEnterprise(String emailEnterprise) {
        this.emailEnterprise = emailEnterprise;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getOwnEnterprise() {
        return ownEnterprise;
    }

    public void setOwnEnterprise(Boolean ownEnterprise) {
        this.ownEnterprise = ownEnterprise;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(Integer sharePrice) {
        this.sharePrice = sharePrice;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }

    @Override
    public String toString() {
        return "Enterprise{" +
                "id=" + id +
                ", emailEnterprise=" + emailEnterprise +
                ", facebook=" + facebook +
                ", twitter=" + twitter +
                ", linkedin=" + linkedin +
                ", phone=" + phone +
                ", ownEnterprise=" + ownEnterprise +
                ", enterpriseName='" + enterpriseName + '\'' +
                ", photo=" + photo +
                ", description='" + description + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", value=" + value +
                ", sharePrice=" + sharePrice +
                ", enterpriseType=" + enterpriseType +
                '}';
    }
}