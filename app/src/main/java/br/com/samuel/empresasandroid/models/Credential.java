package br.com.samuel.empresasandroid.models;

/**
 * @author Samuel Neves
 *         Created on 06/03/18.
 */

public class Credential {

    private static Credential instance;
    private String uid;
    private String client;
    private String accessToken;

    private Credential() {
    }

    public static Credential getInstance() {
        if (instance == null) {
            instance = new Credential();
        }
        return instance;
    }

    @Override
    public String toString() {
        return "Credential{" +
                "uid='" + uid + '\'' +
                ", client='" + client + '\'' +
                ", accessToken='" + accessToken + '\'' +
                '}';
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


}
