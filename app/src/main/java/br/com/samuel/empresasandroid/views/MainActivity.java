package br.com.samuel.empresasandroid.views;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import br.com.samuel.empresasandroid.R;
import br.com.samuel.empresasandroid.databinding.ActivityLoginBinding;

//README
// Este codigo ainda possui diversas falhas de arquitetura visto que estou em um processo de aprendiza-
//do da arquitetura MVVM.
public class MainActivity extends AppCompatActivity {
    public static final String TAG_HOME = "home";
    public static final String TAG_SEARCHING = "search";
    private static final String CURRENT_TAG = TAG_HOME;
    Toolbar toolbar;
    ActivityLoginBinding mBinding;
    Fragment actualFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Attaching the layout to the toolbar object
        toolbar = findViewById(R.id.toolbar_main);
        // Setting toolbar as the ActionBar with setSupportActionBar() call
        setSupportActionBar(toolbar);

        Handler mHandler = new Handler();

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                actualFragment = new MainFragment();
                showFragment(actualFragment, TAG_HOME);
            }
        };

        mHandler.post(mPendingRunnable);
    }

    private void showFragment(Fragment fragment, String tagHome) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        initSearchView(menu);
        return true;
    }

    private void initSearchView(Menu menu) {
        MenuItem searchViewItem = menu.findItem(R.id.menu_search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setQueryHint("Pesquisar");
        if (searchManager != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualFragment = new SearchEnterprisesFragment();
                showFragment(actualFragment, TAG_SEARCHING);
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                actualFragment = new MainFragment();
                showFragment(actualFragment, TAG_HOME);
                return false;
            }
        });
        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                SearchEnterprisesFragment fragment = (SearchEnterprisesFragment) actualFragment;
                fragment.requestEnterprisesToServer(query);
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
    }

}
