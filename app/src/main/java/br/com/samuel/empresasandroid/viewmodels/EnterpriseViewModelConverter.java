package br.com.samuel.empresasandroid.viewmodels;

import br.com.samuel.empresasandroid.models.Enterprise;

/**
 * @author Samuel Neves
 *         Created on 08/03/18.
 */

class EnterpriseViewModelConverter {
    static void convert(Enterprise enterprise, EnterpriseViewModel enterpriseViewModel) {
        if (enterprise.getPhoto() == null || enterprise.getPhoto().isEmpty()) {
            enterpriseViewModel.setPhotoUrl("https://i.imgur.com/bCcizag.png");
        } else {
            enterpriseViewModel.setPhotoUrl(enterprise.getPhoto());
        }
        enterpriseViewModel.setName(enterprise.getEnterpriseName());
        enterpriseViewModel.setType(enterprise.getEnterpriseType().getEnterpriseTypeName());
        enterpriseViewModel.setCountry(enterprise.getCountry());
        enterpriseViewModel.setDescription(enterprise.getDescription());
    }
}
