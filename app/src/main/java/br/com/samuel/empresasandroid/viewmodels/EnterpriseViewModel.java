package br.com.samuel.empresasandroid.viewmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.samuel.empresasandroid.models.Enterprise;

/**
 * @author Samuel Neves
 *         Created on 08/03/18.
 */

public class EnterpriseViewModel {
    @SerializedName("photo")
    @Expose
    private String photoUrl;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("type")
    @Expose
    private String type;
    private String country;

    public EnterpriseViewModel() {

    }

    public EnterpriseViewModel(Enterprise enterprise) {
        EnterpriseViewModelConverter.convert(enterprise, this);
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    void setCountry(String country) {
        this.country = country;
    }

}
