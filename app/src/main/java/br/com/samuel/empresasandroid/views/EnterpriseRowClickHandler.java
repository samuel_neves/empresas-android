package br.com.samuel.empresasandroid.views;

/**
 * @author Samuel Neves
 *         Created on 07/03/18.
 */

public interface EnterpriseRowClickHandler {
    void onEnterpriseRowClick();
}
