package br.com.samuel.empresasandroid.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.samuel.empresasandroid.R;
import br.com.samuel.empresasandroid.models.Credential;
import br.com.samuel.empresasandroid.models.Enterprise;
import br.com.samuel.empresasandroid.models.EnterpriseList;
import br.com.samuel.empresasandroid.models.ServersInfo;
import br.com.samuel.empresasandroid.retrofit.RequestEnterprisesService;
import br.com.samuel.empresasandroid.viewmodels.EnterpriseViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchEnterprisesFragment extends Fragment implements Callback<EnterpriseList> {

    private static final String TAG = SearchEnterprisesFragment.class.getSimpleName();
    ArrayList<EnterpriseViewModel> data;
    private RecyclerView recyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_search_enterprises, container, false);
        recyclerView = fragmentView.findViewById(R.id.recycle_view_enterprises);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.setAdapter(new EnterpriseRecyclerViewAdapter(getActivity(), getData()));

        return fragmentView;
    }

    public void requestEnterprisesToServer(String queryName) {
        Retrofit retrofitInstance = new Retrofit
                .Builder()
                .baseUrl(ServersInfo.serverAddress)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestEnterprisesService requestEnterprisesService = retrofitInstance.create(RequestEnterprisesService.class);
        Log.d(TAG, Credential.getInstance().toString());
        Call<EnterpriseList> enterprisesCall = requestEnterprisesService.getEnterprises(Credential.getInstance().getUid(), Credential.getInstance().getAccessToken(), Credential.getInstance().getClient(), queryName);
        enterprisesCall.enqueue(SearchEnterprisesFragment.this);


    }

    private ArrayList<EnterpriseViewModel> getData() {
        if (data == null)
            data = new ArrayList<>();
        return data;
    }

    public void setData(ArrayList<EnterpriseViewModel> data) {
        this.data = data;
    }

    @Override
    public void onResponse(@NonNull Call<EnterpriseList> call, @NonNull Response<EnterpriseList> response) {
        updateDataOfRecycler(response.body());

    }

    @Override
    public void onFailure(@NonNull Call<EnterpriseList> call, @NonNull Throwable t) {
        Log.d(TAG, t.getMessage());
    }

    private void updateDataOfRecycler(EnterpriseList enterpriseList) {
        data.clear();
        for (Enterprise enterprise : enterpriseList.getEnterprises()) {
            data.add(new EnterpriseViewModel(enterprise));
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }
}
