package br.com.samuel.empresasandroid.views;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import br.com.samuel.empresasandroid.databinding.EnterpriseBinding;
import br.com.samuel.empresasandroid.viewmodels.EnterpriseViewModel;

/**
 * @author Samuel Neves
 *         Created on 07/03/18.
 */

public class EnterpriseRecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder> {
    private Context mContext;
    private ArrayList<EnterpriseViewModel> mList;
    private LayoutInflater inflater;

    EnterpriseRecyclerViewAdapter(Context mContext, ArrayList<EnterpriseViewModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        EnterpriseBinding dataBinding = EnterpriseBinding.inflate(inflater, parent, false);
        return new ViewHolder(dataBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final EnterpriseViewModel viewModel = mList.get(position);
        holder.bind(viewModel);
        final EnterpriseBinding dataBinding = holder.getDataBinding();
        dataBinding.setHandler(new EnterpriseRowClickHandler() {
            @Override
            public void onEnterpriseRowClick() {
                Intent intent = new Intent(mContext, EnterpriseInfoActivity.class);
                Gson gson = new Gson();
                Type type = new TypeToken<EnterpriseViewModel>() {
                }.getType();
                String json = gson.toJson(viewModel, type);
                intent.putExtra("Enterprise", json);
                mContext.startActivity(intent);
            }
        });
        dataBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
