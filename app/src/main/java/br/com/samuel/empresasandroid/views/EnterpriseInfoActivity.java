package br.com.samuel.empresasandroid.views;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import br.com.samuel.empresasandroid.R;
import br.com.samuel.empresasandroid.databinding.ActivityEnterpriseInfoBinding;
import br.com.samuel.empresasandroid.models.Enterprise;
import br.com.samuel.empresasandroid.viewmodels.EnterpriseViewModel;

public class EnterpriseInfoActivity extends AppCompatActivity implements CustomToolbarActivitys {
    private static final String TAG = EnterpriseInfoActivity.class.getSimpleName();

    ActivityEnterpriseInfoBinding mBinding;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initBinding();
        initToolbar();
    }

    private void initBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_enterprise_info);
        EnterpriseViewModel enterprise = getEnterpriseFromIntent();
        mBinding.setViewmodel(enterprise);

    }

    private EnterpriseViewModel getEnterpriseFromIntent() {
        Gson gson = new Gson();
        String stringLocation = getIntent().getStringExtra("Enterprise");
        EnterpriseViewModel enterprise;
        if (stringLocation != null) {
            Type type = new TypeToken<EnterpriseViewModel>() {
            }.getType();
            enterprise = gson.fromJson(stringLocation, type);
        } else {
            Log.d(TAG, "failed");
            enterprise = Enterprise.getErrorEnterprise();
        }
        return enterprise;

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar_enterprise_info);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

}
