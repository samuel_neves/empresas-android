package br.com.samuel.empresasandroid.retrofit;

import br.com.samuel.empresasandroid.models.Credential;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * @author Samuel Neves
 *         Created on 06/03/18.
 */

public interface LoginService {
    @FormUrlEncoded
    @POST("api/v1/users/auth/sign_in")
    Call<Credential> tryLogin(
            @Field("email") String email,
            @Field("password") String password);
}
