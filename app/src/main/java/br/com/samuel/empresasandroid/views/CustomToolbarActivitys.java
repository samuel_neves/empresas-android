package br.com.samuel.empresasandroid.views;

/**
 * @author Samuel Neves
 *         Created on 08/03/18.
 */

public interface CustomToolbarActivitys {
    void initToolbar();
}
