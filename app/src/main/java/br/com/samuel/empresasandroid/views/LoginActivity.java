package br.com.samuel.empresasandroid.views;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import br.com.samuel.empresasandroid.R;
import br.com.samuel.empresasandroid.databinding.ActivityLoginBinding;
import br.com.samuel.empresasandroid.models.Credential;
import br.com.samuel.empresasandroid.models.ServersInfo;
import br.com.samuel.empresasandroid.models.UserLogin;
import br.com.samuel.empresasandroid.retrofit.LoginService;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements Callback<Credential> {

    private static final String TAG = LoginActivity.class.getSimpleName();

    ActivityLoginBinding mBinding;
    UserLogin userLogin;
    ProgressDialogShower progressDialogShower;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDataBinding();

        progressDialogShower = new ProgressDialogShower(this);
    }

    private void initDataBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
//        testeapple@ioasys.com.br/12341234
        userLogin = new UserLogin("", "");
        mBinding.setCredential(userLogin);
        mBinding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, userLogin.toString());
                doLoginRequest();

            }
        });
    }

    private void doLoginRequest() {
        progressDialogShower.showProgressDialog();
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build();

        Retrofit retrofitInstance = new Retrofit
                .Builder()
                .baseUrl(ServersInfo.serverAddress)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        LoginService loginService = retrofitInstance.create(LoginService.class);

        Call<Credential> loginCall = loginService.tryLogin(userLogin.getEmail(), userLogin.getPassword());
        loginCall.enqueue(LoginActivity.this);
    }

    @Override
    public void onResponse(@NonNull Call<Credential> call, @NonNull Response<Credential> response) {
        Log.d(TAG, response.headers().get("Status"));
        progressDialogShower.hideProgressDialog();
        if (response.headers().get("Status") != null && response.code() == 401) {
            authenticationFailed();
        } else if (response.headers().get("Status") != null && response.code() == 200) {
            saveCredentials(response.headers());
        } else {
            errorInRequest();
        }
    }

    private void errorInRequest() {
        Toast.makeText(this, "Some error happened in the request!", Toast.LENGTH_SHORT).show();
    }

    private void authenticationFailed() {
        Toast.makeText(this, "Error in authentication, please verify your inputs.", Toast.LENGTH_SHORT).show();

    }

    private void saveCredentials(Headers headers) {
        Credential.getInstance().setClient(headers.get("client"));
        Credential.getInstance().setAccessToken(headers.get("access-token"));
        Credential.getInstance().setUid(headers.get("uid"));
        Log.d(TAG, Credential.getInstance().toString());

        goToMainActivity();
    }

    @Override
    public void onFailure(@NonNull Call<Credential> call, @NonNull Throwable t) {

        progressDialogShower.hideProgressDialog();
        Toast.makeText(this, "Erro na autenticação!", Toast.LENGTH_SHORT).show();
    }

    private void goToMainActivity() {

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        this.finish();
    }
}
