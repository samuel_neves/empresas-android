package br.com.samuel.empresasandroid.views;

import android.support.v7.widget.RecyclerView;

import br.com.samuel.empresasandroid.databinding.EnterpriseBinding;
import br.com.samuel.empresasandroid.viewmodels.EnterpriseViewModel;


/**
 * @author Samuel Neves
 *         Created on 07/03/18.
 */

class ViewHolder extends RecyclerView.ViewHolder {
    private EnterpriseBinding mDataBinding;


    ViewHolder(EnterpriseBinding dataBinding) {
        super(dataBinding.getRoot());
        this.mDataBinding = dataBinding;

    }

    void bind(EnterpriseViewModel viewModel) {
        this.mDataBinding.setViewmodel(viewModel);
    }

    EnterpriseBinding getDataBinding() {
        return mDataBinding;
    }


}
