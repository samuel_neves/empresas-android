package br.com.samuel.empresasandroid.retrofit;

import br.com.samuel.empresasandroid.models.EnterpriseList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * @author Samuel Neves
 *         Created on 07/03/18.
 */

public interface RequestEnterprisesService {
    //
    @GET("/api/v1/enterprises?")
    Call<EnterpriseList> getEnterprises(@Header("uid") String uid, @Header("access-token") String accessToken, @Header("client") String client, @Query("name") String nameOfEnterprise);

}
